<?php 
class Valkryine
{
    public function __construct()
    {
        
    }
    public function GetFonts(){
        $imagick = "F:/static/vishnu/fonts";
        $data   = glob("$imagick/*.ttf");
        $iemc = [];
        foreach ($data as $key => $value) {
            $iem[] =  str_replace($imagick."/","",$value);
        }
        return $iem;
    }
    public function GetImages(){
        $imagick = "images";
        $data   = glob("$imagick/*.jpg");
        $iemc = [];
        foreach ($data as $key => $value) {
            $iem[] =  str_replace($imagick."/","",$value);
        }
        return $iem;
    }
    public function GetRTO(){
        $rto = [];
        for ($i=1; $i <= 99; $i++) { 
            $rto[] = str_pad($i, 2, "0", STR_PAD_LEFT);
        }
        return $rto;
    }
    public function GetSeries()
    {
        $code           = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $size           = 2;
        $current_set    = array('');

        for ($i = 0; $i < $size; $i++) {
            $tmp_set = [];
            foreach ($current_set as $curr_elem) {
                foreach ($code as $new_elem) {
                    $tmp_set[] = $curr_elem . $new_elem;
                }
            }
            $current_set = $tmp_set;
        }
        foreach ($code  as $key ) {
            $current_set[] = $key;
        }
        return $current_set;
    }
    
}
    ?>