<?php

ini_set('memory_limit', "2G");
ini_set('max_execution_time', "3600");

include "Algorithm.php";

$state = $_POST['state'];
$rto = $_POST['rto'];
$start = $_POST['start'];
$end = $_POST['end'];
$series = $_POST['series'];
$image = $_POST['image'];
$font = $_POST['font'];

$algorithm = new Algorithm;
$algorithm->state = $state;
$algorithm->rto = $rto;
$algorithm->starts = $start;
$algorithm->ends = $end;
$algorithm->series = $series;
$algorithm->image = $image;
$algorithm->font = $font;

$registrations = $algorithm->GetRegistrations();

$tempDirectory = uniqid();
mkdir($tempDirectory);

foreach ($registrations as $registration) {
    $algorithm->SaveImage("arial.ttf", "$state $rto $registration", $tempDirectory);
}

$zipFileName = "$state$rto$series.zip";
$zip = new ZipArchive;
$zip->open($zipFileName, ZipArchive::CREATE);

foreach (glob("$tempDirectory/*") as $file) {
    $zip->addFile($file);
}

$zip->close();

foreach (glob("$tempDirectory/*") as $file) {
    unlink($file);
}

rmdir($tempDirectory);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$zipFileName.'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($zipFileName));
readfile($zipFileName);

exit;
