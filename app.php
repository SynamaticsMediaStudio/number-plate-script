<?php 
#zH7S95bUU-Zn
ini_set('memory_limit', "2G");
ini_set('max_execution_time', "3600");
error_reporting(E_ALL);
ini_set('display_errors', 1);
putenv('GDFONTPATH=F:/static/vishnu/fonts');        

$folder      = uniqid();
mkdir($folder);
$start       = $_POST['start'];//Se
$ended       = $_POST['end'];//Se
$district    = $_POST['state'];//Se
$state       = $_POST['rto']; //Se
$series      = $_POST['series']; //Se
$img         = $_POST['image']; //Se
$font        = $_POST['font']; //Se
$fontsize    = $_POST['fontsize'];//Se
$rotation    = $_POST['rotation'];//Se
$left        = $_POST['left'];//Se
$top         = $_POST['top'];//Se
$break       = (isset($_POST['break'])) ? $_POST['break']:"false";//Se


require "Valkryine.php";
$data = [];
$application = $ended - $start+1;

for ($i=0; $i < $application; $i++) {
    $item = $start+$i; 
    $data[] = "$district $state $series $item";
}

for ($i=0; $i < count($data); $i++) { 
    $key = $data[$i];
    $jpg_image = imagecreatefromjpeg('images/'.$img);
    $white = imagecolorallocate($jpg_image, 0, 0, 0);

    if($break == "true"){
      $text1 = substr($key,0,5);
      $text2 = substr($key,6,13);
      imagettftext($jpg_image, $fontsize, $rotation, $left, $top, $white, $font, $text1);
      imagettftext($jpg_image, $fontsize, $rotation, $left, $top+$fontsize+50, $white, $font, $text2);
    }
    else{
      $text = $key;
      imagettftext($jpg_image, $fontsize, $rotation, $left, $top, $white, $font, $text);
    }

    $filename = str_replace(' ','',$key).'.jpg';
    imagejpeg( $jpg_image, $folder."/".$filename );
    imagedestroy($jpg_image);
}



$fzip = $district.$state.$series.$start.$ended.".zip";

$zip = new ZipArchive;
$zip->open("$fzip", ZipArchive::CREATE);
foreach (glob("$folder/*") as $file) {
    $zip->addFile($file);
}

$zip->close();
foreach (glob("$folder/*") as $file) {
    unlink($file);
}

rmdir($folder);
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$fzip.'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($fzip));
readfile($fzip);