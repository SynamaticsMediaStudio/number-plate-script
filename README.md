# Number Plate Script

This is a PHP script that generates number plates in the form of `state code + RTO code + sequence number`. It uses the `arial.ttf` font to render the numbers on an image. The images are then compressed into a zip file for easy download.

## Installation

1. Clone this repository
2. Install [Composer](https://getcomposer.org/)
3. Run `composer install` to install the required dependencies
4. Copy the `config.example.php` file to `config.php` and update the configuration

## Usage

1. Open the `index.php` file in your web browser
2. Select the state and RTO code from the drop down menus
3. Set the starting and ending sequence numbers, and the series (if any)
4. Choose the font and image to use
5. Select the output format (HTML or ZIP)
6. Click the "Generate" button to generate the number plates
7. If the output format is set to ZIP, the script will download a zip file containing all the generated images

## Configuration

You can configure the script by editing the `config.php` file. Here are the available options:

- `font_path`: The path to the `arial.ttf` font file
- `images_path`: The path to the directory where the generated images will be saved
- `zip_path`: The path to the directory where the generated zip file will be saved
- `template_path`: The path to the HTML template file used to display the generated number plates
- `max_zip_size`: The maximum size of the generated zip file in bytes. If the file size exceeds this limit, the script will not generate the zip file

## Troubleshooting

If you encounter any issues while using the script, you can try the following:

- Make sure that the `arial.ttf` font file is in the correct path and readable by the server
- Check the error logs for any PHP errors or warnings
- Increase the `max_execution_time` and `memory_limit` settings in `php.ini` if you are generating a large number of number plates

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more information.

