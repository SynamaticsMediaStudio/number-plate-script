<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ML Gen</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->    
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark py-0">
            <a class="navbar-brand" href="#">Static Image Generator</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
                </ul>
            </div>
            </nav>

            <?php include 'Valkryine.php';$algorithm = new Valkryine;?>


            <div class="container">
                <div class="py-4"></div>
                <div class="py-4"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <form action="app.php" method="post" class="form row">
                                    <div class="form-group col-md-12">
                                        <label for="input-item-one">State (Eg: KL )</label>
                                        <input type="text" name="state" class="form-control" id="input-item-one" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="rto">RTO</label>
                                        <select class="custom-select" name="rto" id="rto" required>
                                            <option disabled required selected>--Select--</option>
                                            <?php foreach ($algorithm->GetRTO() as $key ) {
                                                echo "<option value='$key'>$key</option>";
                                            }?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="series">Series</label>
                                        <select class="custom-select" name="series" id="series" required>
                                            <option disabled required selected>--Select--</option>
                                            <?php foreach ($algorithm->GetSeries() as $key ) {
                                                echo "<option value='$key'>$key</option>";
                                            }?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="start">Starting Series (Eg: 1234 )</label>
                                        <input type="text" name="start" class="form-control" id="start" required>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label for="end">Ending Series (Eg: 4321 )</label>
                                        <input type="text" name="end" class="form-control" id="end" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="checkbox" name="break" class="" id="break" value="true">
                                        <label for="break">Line Break? </label>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <input type="number" name="fontsize" class="form-control" id="fontsize" required>
                                        <label for="fontsize">Font Size </label>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <input type="number" name="rotation" class="form-control" id="rotation" required>
                                        <label for="rotation">Rotation </label>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <input type="number" name="left" class="form-control" id="left" required>
                                        <label for="left">Left </label>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <input type="number" name="top" class="form-control" id="top" required>
                                        <label for="top">Top </label>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label for="font">Font</label>
                                        <select class="custom-select" name="font" id="font" required>
                                            <option disabled required selected>--Select--</option>
                                            <?php foreach ($algorithm->GetFonts() as $key ) {
                                                echo "<option value='$key'>$key</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="image">Select Image</label>
                                        <select class="custom-select" name="image" id="image" required>
                                            <?php foreach ($algorithm->GetImages() as $key) { ?>
                                                <option value="<?php echo $key;?>"><?php echo $key;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="submit" class="btn btn-outline-primary">Download Images</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <img src="" alt="" id="preview" class="img-fluid">
                </div>
                </div>
            </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js" async defer></script>
<script>
    $(function(){
        $(document).on("change keyup keydown","select,input",function(){
            var state = $('[name=state]').val();
            var rto = $('[name=rto]').val();
            var font = $('[name=font]').val();
            var series = $('[name=series]').val();
            var start = $('[name=start]').val();
            var end = $('[name=end]').val();
            var image = $('[name=image]').val();
            var fontsize = $('[name=fontsize]').val();
            var rotation = $('[name=rotation]').val();
            var left = $('[name=left]').val();
            var top = $('[name=top]').val();
            var brk = $('[name=break]:checked').val();
            var data = {"break":brk,"string":state+' '+rto+' '+series+' '+start,"font":font,"image":image,"fontsize":fontsize,"rotation":rotation,"left":left,"top":top,}
            var URLIMG = "image.php?"+$.param( data, true )
            console.log(URLIMG)
            $("#preview").attr("src",URLIMG)
            })
    })
</script>



    </body>
</html>