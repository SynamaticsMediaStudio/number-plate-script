<?php

class Algorithm {
    var $state;
    var $rto;
    var $starts;
    var $ends;
    var $image;
    var $font;
    var $series;
    var $fonts;
    var $fontsize;
    var $rotation;
    var $left;
    var $top;
    var $break;
    var $folder;
    var $string;

    public function __construct()
    {
        $this->fonts = "F:/static/vishnu/fonts";
    }
    private function Adapt(){
        $data = [];
        $application = $this->ends - $this->starts+1;
        for ($i=0; $i < $application; $i++) { 
            $data[] = $this->starts+$i;
        }
        return $data;
    }
    public function GetSeries()
    {
        $code           = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $size           = 2;
        $current_set    = array('');

        for ($i = 0; $i < $size; $i++) {
            $tmp_set = [];
            foreach ($current_set as $curr_elem) {
                foreach ($code as $new_elem) {
                    $tmp_set[] = $curr_elem . $new_elem;
                }
            }
            $current_set = $tmp_set;
        }
        foreach ($code  as $key ) {
            $current_set[] = $key;
        }
        return $current_set;
    }
    public function GetRegistrations()
    {
        $adpats  =  $this->Adapt();
        $element = [];
        foreach ($adpats as $value) {
            $element[] = $this->series." ".$value;
        }
        return $element;
    }
    public function SaveImage($font,$text,$folder)
    {
        $image = $this->image;
        putenv('GDFONTPATH='.$this->fonts);
        $string = $this->string;
        $image = $this->image;
        $font = $this->font;
        $fontsize = $this->fontsize;
        $rotation = $this->rotation;
        $left = $this->left;
        $top = $this->top;
        $break = $this->break;
        
        $jpg_image = imagecreatefromjpeg('images/'.$image);
  
        $white = imagecolorallocate($jpg_image, 0, 0, 0);
  
        if($break == "true"){
          $text1 = substr($string,0,5);
          $text2 = substr($string,6,13);
          imagettftext($jpg_image, $fontsize, $rotation, $left, $top, $white, $font, $text1);
          imagettftext($jpg_image, $fontsize, $rotation, $left, $top+$fontsize+50, $white, $font, $text2);
        }
        else{
          $text = $string;
          imagettftext($jpg_image, $fontsize, $rotation, $left, $top, $white, $font, $text);
        }
        
        $filename = str_replace(' ','',$string).'.jpg';
        imagejpeg( $image, $this->folder."/".$filename );
        imagedestroy($jpg_image);


    }
    public function GetFonts(){
        $imagick = $this->fonts;
        $data   = glob("$imagick/*.ttf");
        $iemc = [];
        foreach ($data as $key => $value) {
            $iem[] =  str_replace($imagick."/","",$value);
        }
        return $iem;
    }
    public function GetRTO(){
        $rto = [];
        for ($i=1; $i <= 99; $i++) { 
            $rto[] = str_pad($i, 2, "0", STR_PAD_LEFT);
        }
        return $rto;
    }
}
